﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace OutTest {
    public class Logic {
        public static List<String> ExtractDataNames(DataTable dt, List<String> wordsNames) {

            try {

                foreach (DataRow dr in dt.Rows) {
                    wordsNames.Add(dr[0].ToString());
                    wordsNames.Add(dr[1].ToString());
                }
                return wordsNames;

            }
            catch (Exception x) {
                throw x;
            }


        }
        public static List<String> ExtractDataAddress(DataTable dt, List<String> wordsAddress) {
            try {
                foreach (DataRow dr in dt.Rows) {
                    wordsAddress.Add(dr[2].ToString());
                }
                return wordsAddress;
            }
            catch (Exception x) {
                throw x;
            }

        }
        public static List<String> ExtractAddressData(DataTable dt, List<String> words) {
            try {
                foreach (DataRow dr in dt.Rows) {
                    words.Add(dr[2].ToString());
                }
                return words;
            }
            catch(Exception x) {
                throw x;
            }

        }
        public static List<AccoranceNames> FillDataNamesObject(List<String> words, List<AccoranceNames> wordsAccorabce) {
            try {
                foreach (var str in words) {
                    int count;
                    count = words.Where(x => x.Equals(str)).Count();
                    bool match = wordsAccorabce.Any(a => a.Name == str);
                    if (!match) {
                        wordsAccorabce.Add(new AccoranceNames { Name = str, Freqency = count });

                    }
                }
                return wordsAccorabce;
            }
            catch(Exception x) {
                throw x;
            }


        }

        public static List<Address> FillDataAddressObject(List<String> wordsAddress, List<Address> address) {
            try {
                foreach (var str in wordsAddress) {
                    int SpaceIndex = str.ToString().IndexOf(" ");
                    address.Add(new Address { StreetNumber = str.ToString().Substring(0, SpaceIndex), StreetName = str.ToString().Substring(SpaceIndex + 1) });

                }
                return address;

            }
            catch(Exception x) {
                throw x;
            }

        }

        public static void CreateNamesFile(List<String> wordsAddress, List<AccoranceNames> wordsAccorabce) {

            try {
                var OrderedwordsAccorabce = wordsAccorabce.OrderByDescending(t => t.Freqency).ThenBy(a => a.Name);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "Names.txt", false)) {
                    foreach (var Ordered in OrderedwordsAccorabce) {
                        file.WriteLine(Ordered.Name + " " + Ordered.Freqency);
                    }
                }
            }
            catch(Exception x)
            {
                throw x;
            }


        }

        public static void CreateAddressFile(List<String> wordsAddress, List<Address> address) {

            try {
                var AddressOrder = address.OrderBy(a => a.StreetName);

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "Address.txt", false)) {

                    foreach (var add in AddressOrder) {
                        file.WriteLine(add.StreetNumber + " " + add.StreetName);
                    }
                }
            }
            catch(Exception x) {
                throw x;
            }

            
        }
    }
}
