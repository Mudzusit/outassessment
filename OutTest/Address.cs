﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutTest {
    public class Address {
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
    }
}
