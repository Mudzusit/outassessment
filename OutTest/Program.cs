﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Odbc;
using System.Data;

namespace OutTest {
    class Program {
        static void Main(string[] args) {
            DataTable dt = new DataTable();

            try {
                dt = DataSourceConnection.Connect(Directory.GetCurrentDirectory(), "OutsursanceAssessment");

                List<AccoranceNames> wordsAccorabce = new List<AccoranceNames>();
                List<Address> address = new List<Address>();
                List<String> wordsNames = new List<string>();
                List<String> wordsAddress = new List<string>();

                wordsNames = Logic.ExtractDataNames(dt, wordsNames);
                wordsAccorabce = Logic.FillDataNamesObject(wordsNames, wordsAccorabce);


                wordsAddress = Logic.ExtractDataAddress(dt, wordsAddress);
                address = Logic.FillDataAddressObject(wordsAddress, address);

                Logic.CreateNamesFile(wordsNames, wordsAccorabce);

                Console.WriteLine();

                Console.WriteLine("Files Created in Debug/release folder of the project");

                Logic.CreateAddressFile(wordsAddress, address);

                Console.ReadLine();
            }
            catch(Exception x) {
                Console.WriteLine(x.Message);
                Console.ReadLine();
            }



        }

        

    }
}
