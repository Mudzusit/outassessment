﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Odbc;
using System.Data;

namespace OutTest {
   public class DataSourceConnection {
        public static DataTable Connect(string FileLocation, string FileName) {

            try {
                DataTable dt = new DataTable();
                using (OdbcConnection conn = new OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + FileLocation + ";Extensions=asc,csv,tab,txt")) {
                    using (OdbcCommand cmd =
                      new OdbcCommand("SELECT * FROM " + FileName + ".csv", conn)) {
                        conn.Open();

                        using (OdbcDataReader dr =
                          cmd.ExecuteReader(CommandBehavior.SequentialAccess)) {
                            dt.Load(dr);
                            return dt;
                        }
                    }
                }
            }
            catch (Exception x) {
                throw x;
            }


        }
    }
}
