﻿using System;
using OutTest;
using System.IO;
using System.Data;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest {
    [TestClass]
    public class UnitTest1 {
        DataTable dt = new DataTable();
        List<Address> address = new List<Address>();
        List<String> wordsNames = new List<string>();
        List<String> wordsAddress = new List<string>();
        List<AccoranceNames> wordsAccorabce = new List<AccoranceNames>();
        [TestMethod]
        public void FileConnectTest() {
            dt = DataSourceConnection.Connect(Directory.GetCurrentDirectory(), "OutsursanceAssessment");
            wordsNames = Logic.ExtractDataNames(dt, wordsNames);
        }
        [TestMethod]
        public void FillDataNamesObjectTest() {
            Logic.FillDataNamesObject(wordsNames, wordsAccorabce);
        }

        [TestMethod]
        public void ExtractDataNamesTest() {
            wordsNames = Logic.ExtractDataNames(dt, wordsNames);
        }

        [TestMethod]
        public void ExtractDataAddressTest() {
            wordsAddress = Logic.ExtractDataAddress(dt, wordsAddress);
        }

        [TestMethod]
        public void FillDataAddressObject() {
            address = Logic.FillDataAddressObject(wordsAddress, address);
        }
        [TestMethod]
        public void CreateNamesFileTest() {
            Logic.CreateNamesFile(wordsNames, wordsAccorabce);
        }
        [TestMethod]
        public void CreateAddressFileTest() {
            Logic.CreateAddressFile(wordsAddress, address);
        }

    }


} 
